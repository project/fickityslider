CONTENTS OF THIS FILE
=====================
 * Introduction
 * Requirements
 * Installation
 * Configuration

INRODUCTION
===========

This module integrates the Flickity library with Drupal which allows you to create responsive and easy-to-use slideshow.

REQUIREMENTS
============

This module requires following contributed module:
 * jQuery Update (https://www.drupal.org/project/jquery_update).

INSTALLATION
============

1) Install and enable the module together with jQuery Update module.

2) Go to admin/structure/types, this module will create a content type "Flickity Slider".

3) Go to node/add page or click "Add Content" link and add content of type "Flickity Slider".

4) Add Title and Upload an image.

5) Go to admin/structure/block and enable the "Flickity Slider" in any of your theme regions.

6) By default the slider will be displayed on the front page of the website.

7) This module uses "flickity_slider_style" image style.

CONFIGURATION
=============

* Flickity Slider settings can be found at admin/config/media/flickityslider.
